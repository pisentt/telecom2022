clc;
clear;
close all;

%Variables définies
Fe = 24000;
% Te = 1/Fe;
Rb = 6000;
% Tb = 1/Rb;
% BW = 1000;
Nbits = 100026;
phi = 1.75; % 100°
%phi = 0.70; % 40°
SNR = 50;

suite_binaire = autres('generer_suite_binaire',Nbits);

%% Chaine sans bruit sans phase

% signal_module = mod_part3('modulateur1', Fe, Rb, suite_binaire);
% mod_part3('etude_demodulateur_1', suite_binaire, Fe, Rb, signal_module,0);

%% Chaine sans bruit avec phase

% signal_module = mod_part3('modulateur1', Fe, Rb, suite_binaire);
% mod_part3('etude_demodulateur_1', suite_binaire, Fe, Rb, signal_module,phi);

%% Chaine avec bruit avec phase

% signal_module = mod_part3('modulateur1', Fe, Rb, suite_binaire);
% mod_part3('etude_demodulateur_bruit',suite_binaire, Fe, Rb, signal_module, phi, SNR);
% mod_part3('comparaison_teb',suite_binaire, Fe, Rb,phi);

%% Correction de l'erreur de phase

% signal_module = mod_part3('modulateur1', Fe, Rb, suite_binaire);
% mod_part3('etude_demodulateur_corrige',suite_binaire, Fe, Rb, signal_module, phi, SNR);
% mod_part3('comparaison_teb_correction',suite_binaire, Fe, Rb,phi);

%% Utilisation d'un codage par transition

signal_module = mod_part3('modulateur_transition', Fe, Rb, suite_binaire);
mod_part3('etude_demodulateur_corrige_transition',suite_binaire, Fe, Rb, signal_module, phi, SNR);
mod_part3('comparaison_teb_correction_transition',suite_binaire, Fe, Rb,phi);