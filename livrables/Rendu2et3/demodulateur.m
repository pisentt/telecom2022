% Ce fichier contient toutes les fonctions en lien avec la génération du
% signal modulé.

function varargout = demodulateur(nom_fonction,varargin)

  switch nom_fonction
    case 'filtre_reception_mod1'
      % Générer un signal avec le modulateur1 de la partie 1 
      % et le demodulateur de la partie 3.
      [varargout{1}, varargout{2}] = filtre_reception_mod1(varargin{:});

    case 'etude_demodulateur_sans_canal'
      % Tracer le signal du demodulateur et la fonction g = h * hr
      etude_demodulateur_sans_canal(varargin{:});

    case 'filtre_reception_mod1_canal'
      % Générer un signal avec le modulateur1 de la partie 1 
      % et le demodulateur de la partie 1 avec un canal pris en compte.
      [varargout{1}, varargout{2}, varargout{3}] = filtre_reception_mod1_canal(varargin{:});

    case 'etude_demodulateur_avec_canal'
      % Tracer le signal du demodulateur et la fonction 
      % g = h * hr * hc
      etude_demodulateur_avec_canal(varargin{:});

    case 'etude_chaine_transmission'
      % Etude d'une chaine de transmission (partie 4)
      etude_chaine_transmission(varargin{:});

    case 'etude_chaine_bruit'
      % Etude de la chaîne avec le bruit (partie 4)
      etude_chaine_bruit(varargin{:});

    case 'calcul_teb'
      % Calcul du TEB théorique (partie 4)
      varargout{1} = calcul_teb(varargin{:});

    case 'comparaison_teb'
      % Comparaison TEB exp et théo (partie 4)
      comparaison_teb(varargin{:});

    case 'comparaison_chaines'
      % Comparer les TEB de deux chaines entre eux sur une même figure
      % (partie 4.2)
      comparaison_chaines(varargin{:});
  end

end


function [signal_filtre, g] = filtre_reception_mod1(signal_module, Ns)
  h  = ones(1,Ns);
  hr = ones(1,Ns);
  g  = conv(h,hr);

  % on filtre
  signal_filtre = filter(hr,1,signal_module);
end


function [signal_filtre, g] = filtres_reception_chaine2(signal_module, Ns)
  h  = ones(1,Ns);
  hr = ones(1,Ns/2);
  g  = conv(h,hr);

  % on filtre
  signal_filtre = filter(hr,1,signal_module);
end


function suite_binaire = decode_signal_filtre(signal_filtre, instant_echant, Ns)

  % On échantillonne sur le premier bit
  suite_binaire = reshape(signal_filtre,Ns,[]);
  suite_binaire = suite_binaire(instant_echant,:) > 0;
end


function suite_binaire = decode_signal_filtre_4motifs(signal_filtre, instant_echant, Ns)

  % On échantillonne sur le premier bit
  suite_binaire = reshape(signal_filtre,Ns,[]);
  suite_echantillonnee = suite_binaire(instant_echant,:);

  motif1 = suite_echantillonnee < -32;
  motif4 = suite_echantillonnee > 32;
  motif2 = (suite_echantillonnee < 0) - motif1;
  motif3 = (suite_echantillonnee > 0) - motif4;

  bin1 = motif1' * [0 0];
  bin2 = motif2' * [0 1];
  bin3 = motif3' * [1 1];
  bin4 = motif4' * [1 0];

  suite_binaire = bin1 + bin2 + bin3 + bin4;
  suite_binaire = reshape(suite_binaire', 1, []);

end


function etude_demodulateur_sans_canal(suite_binaire, Fe, Rb, signal_module)
  Te = 1/Fe;
  Tb = 1/Rb;
  Ts = Tb;
  Ns = floor(Ts/Te);

  [signal_filtre, g] = filtre_reception_mod1(signal_module, Ns);

  t  = 0:Te:(length(signal_filtre)-1)*Te;

  figure();
  plot(t, signal_filtre);
  title("Tracé du signal en sortie du filtre de réception");
  xlabel("Temps (s)");

  figure();
  plot(g);
  title("Tracé des points de g");
  xlabel("indice du point");

  % On trace le diagramme de l'oeil avec la formule de l'énoncé
  % On enlève les Ns premiers points pour eviter une ligne parasite
  figure();
  oeil = reshape(signal_filtre, Ns, []);
  plot(oeil(:,2:end));
  title("Diagramme de l'oeil");
  xlabel("indice du point");

  suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 3, Ns);

  % On trace la suite binaire retrouvée par le démodulateur
  figure();
  plot(suite_binaire_reconstruite);
  title("suite binaire reconstruite par le démodulateur 1");

  disp("Taux erreur démodulateur 1");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))

  %% Réponse à la question 3.1−2)
  % avec n0 = 3 on trouve un TEB aux alentours de 0.5, cela s'explique
  % car n0 = 3 ne respecte pas le critère de Nyquist. On doit prendre
  % n0 = 8 en regardant le diagramme de l'œil.
end

function [signal_filtre, g, hc] = filtre_reception_mod1_canal(Fe, signal_module, f0, Ns)
  Te = 1 / Fe;
  h = ones(1, Ns);
  hr = ones(1, Ns);

  ordre = 61;
  t  = -Te*(ordre-1)/2:Te:Te*(ordre-1)/2;

  hc = 2*f0*sinc(f0*t);
  g = conv(h,hc);
  g = conv(g,hr);


  % On ajoute des zeros pour gerer le retard
  signal_module = [signal_module zeros(1,(ordre - 1)/2)];

  signal_filtre = filter(hc,1,signal_module);

  signal_filtre = signal_filtre((ordre - 1)/2 + 1:end);

  signal_filtre = filter(hr,1,signal_filtre);
end

function etude_demodulateur_avec_canal(suite_binaire, Fe, Rb, signal_module, f0)
  Te = 1/Fe;
  Tb = 1/Rb;
  Ts = Tb;
  Ns = floor(Ts/Te);
  N_fft = 2048;


  [signal_filtre, g, hc] = filtre_reception_mod1_canal(Fe, signal_module, f0, Ns);
  % On récupère g2 = h * hr de la chaine sans canal afin de calculer
  % |H(f)Hr(f)|
  [~, g2] = filtre_reception_mod1(signal_module, Ns);

  t  = 0:Te:(length(signal_filtre)-1)*Te;

  figure();
  plot(t, signal_filtre);
  title("Tracé du signal en sortie du filtre de réception avec canal");
  xlabel("Temps (s)");

  figure();
  plot(g);
  title("Tracé des points de g avec canal");
  xlabel("indice du point");

  % On trace le diagramme de l'oeil avec la formule de l'énoncé
  % On enlève les Ns premiers points pour eviter une ligne parasite
  figure();
  oeil = reshape(signal_filtre, Ns, []);
  plot(oeil(:,2:end));
  title("Diagramme de l'oeil du démodulateur avec canal");
  xlabel("indice du point");


  suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);


  % On trace la suite binaire retrouvée par le démodulateur
  figure();
  plot(suite_binaire_reconstruite);
  title("suite binaire reconstruite par le démodulateur avec canal");

  disp("Taux erreur démodulateur avec canal");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))

  % Tracé des réponses fréquentielles
  % On récupère g2 = h * hr de la chaine sans canal afin de calculer
  % |H(f)Hr(f)|
  HHr = fft(g2,N_fft);
  F = linspace(-Fe/2,Fe/2,N_fft);


  figure();
  semilogy(F, fftshift(abs(HHr)) / max(abs(HHr)));
  xlabel("Fréquence (Hz)");

  hold on;
  Hc = fft(hc, N_fft);
  semilogy(F,fftshift(abs(Hc)) / max(abs(Hc)));
  xlabel("Fréquence (Hz)");
  legend("Tracé de |H(f)Hr(f)| (normalisé)", "Tracé de |Hc(f)| (normalisé)");

  title("Comparaison des réponses fréquentielles du canal et du système sans canal");
  grid on;

end



function etude_chaine_transmission(suite_binaire, nom_chaine, Fe, Rb, signal_module)
  Te = 1/Fe;
  Tb = 1/Rb;

  switch nom_chaine
    case 'chaine1'
      Ts = Tb;
      Ns = floor(Ts/Te);
      [signal_filtre, g] = filtre_reception_mod1(signal_module, Ns);
    case 'chaine2'
      Ts = Tb;
      Ns = floor(Ts/Te);
      [signal_filtre, g] = filtres_reception_chaine2(signal_module, Ns);
    case 'chaine3'
      Ts = 2*Tb;
      Ns = floor(Ts/Te);
      [signal_filtre, g] = filtre_reception_mod1(signal_module, Ns);
  end

  t  = 0:Te:(length(signal_filtre)-1)*Te;

  figure();
  plot(t, signal_filtre);
  title("Tracé du signal en sortie du filtre de réception sans bruit");
  xlabel("Temps (s)");

  figure();
  plot(g);
  title("Tracé des points de g sans bruit");
  xlabel("indice du point");

  % On trace le diagramme de l'oeil avec la formule de l'énoncé
  % On enlève les Ns premiers points pour eviter une ligne parasite
  figure();
  oeil = reshape(signal_filtre, Ns, []);
  plot(oeil(:,2:end));
  title("Diagramme de l'oeil du démodulateur sans bruit");
  xlabel("indice du point");


  switch nom_chaine
    case 'chaine1'
      suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
    case 'chaine2'
      suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
    case 'chaine3'
      suite_binaire_reconstruite = decode_signal_filtre_4motifs(signal_filtre, 16, Ns);
  end

  % On trace la suite binaire retrouvée par le démodulateur
  figure();
  plot(suite_binaire_reconstruite);
  title("suite binaire reconstruite par le démodulateur sans bruit");

  disp("Taux erreur " + nom_chaine);
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))


end

function signal_filtre = filtre_reception_mod1_bruit(signal_module, Ns, SNR)
  hr = ones(1,Ns);
  signal_bruite = signal_module + bruit(signal_module, Ns, 2, SNR);

  % on filtre
  signal_filtre = filter(hr,1,signal_bruite);
end

function signal_filtre = filtre_reception_chaine2_bruit(signal_module, Ns, SNR)
  hr = ones(1,Ns/2);
  signal_bruite = signal_module + bruit(signal_module, Ns, 2, SNR);

  % on filtre
  signal_filtre = filter(hr,1,signal_bruite);
end

function signal_filtre = filtre_reception_chaine3_bruit(signal_module, Ns, SNR)
  hr = ones(1,Ns);
  signal_bruite = signal_module + bruit(signal_module, Ns, 4, SNR);

  % on filtre
  signal_filtre = filter(hr,1,signal_bruite);
end


function etude_chaine_bruit(suite_binaire, nom_chaine, Fe, Rb, signal_module, SNR)
  Te = 1/Fe;
  Tb = 1/Rb;

  switch nom_chaine
    case 'chaine1'
      Ts = Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_mod1_bruit(signal_module, Ns, SNR);
    case 'chaine2'
      Ts = Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_chaine2_bruit(signal_module, Ns, SNR);
    case 'chaine3'
      Ts = 2*Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_chaine3_bruit(signal_module, Ns, SNR);
  end

  t  = 0:Te:(length(signal_filtre)-1)*Te;

  figure();
  plot(t, signal_filtre);
  title("Tracé du signal en sortie du filtre de réception avec bruit");
  xlabel("Temps (s)");


  % On trace le diagramme de l'oeil avec la formule de l'énoncé
  % On enlève les Ns premiers points pour eviter une ligne parasite
  figure();
  oeil = reshape(signal_filtre, Ns, []);
  plot(oeil(:,2:end));
  title("Diagramme de l'oeil du démodulateur avec bruit");
  xlabel("indice du point");


  switch nom_chaine
    case 'chaine1'
      suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
    case 'chaine2'
      suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
    case 'chaine3'
      suite_binaire_reconstruite = decode_signal_filtre_4motifs(signal_filtre, 16, Ns);
  end

  % On trace la suite binaire retrouvée par le démodulateur
  figure();
  plot(suite_binaire_reconstruite);
  title("suite binaire reconstruite par le démodulateur avec bruit");

  disp("Taux erreur " + nom_chaine);
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))
end


function n = bruit(x,Ns,M,SNR)
  P_x = mean(abs(x).^2);
  sigma_n = sqrt(P_x*Ns/(2*log2(M)*SNR));
  n = sigma_n * randn(1,length(x));
end


function comparaison_teb(suite_binaire, Fe, Rb, nom_chaine)
  figure();
  a = 0:8;
  x = 10.^(a / 10);

    switch nom_chaine
      case 'chaine1'
        signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
        TEB = calcul_teb('chaine1', x);
        semilogy(a, TEB);
        hold on;
        TEB_exp = calcul_teb_exp('chaine1', suite_binaire, Fe, Rb, signal_module, x);
        semilogy(a, TEB_exp);
        legend('theo chaine 1', 'exp chaine 1');
      case 'chaine2'
        signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
        TEB = calcul_teb('chaine2', x);
        semilogy(a, TEB);
        hold on;
        TEB_exp = calcul_teb_exp('chaine2', suite_binaire, Fe, Rb, signal_module, x);
        semilogy(a, TEB_exp);
        legend('theo chaine 2', 'exp chaine 2');
      case 'chaine3'
        signal_module = modulateur('modulateur2', Fe, Rb, suite_binaire);
        TEB = calcul_teb('chaine3', x);
        semilogy(a, TEB);
        hold on;
        TEB_exp = calcul_teb_exp('chaine3', suite_binaire, Fe, Rb, signal_module, x);
        semilogy(a, TEB_exp);
        legend('theo chaine 3', 'exp chaine 3');
    end
  end


  function TEB = calcul_teb(nom_chaine,SNR)
    switch nom_chaine
      case 'chaine1'
        TES = qfunc(sqrt(2*SNR));
        TEB = TES;
      case 'chaine2'
        TES = qfunc(sqrt(SNR));
        TEB = TES;
      case 'chaine3'
        TES = qfunc(sqrt(4/5*SNR));
        TEB = 3/4*TES;
    end

  end

  function TEB_exp = calcul_teb_exp(nom_chaine, suite_binaire, Fe, Rb, signal_module, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;

    y = zeros(1,length(SNR));
    i = 1;
    for snr = SNR
      switch nom_chaine
        case 'chaine1'
          Ts = Tb;
          Ns = floor(Ts/Te);
          signal_filtre = filtre_reception_mod1_bruit(signal_module, Ns, snr);
          suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
        case 'chaine2'
          Ts = Tb;
          Ns = floor(Ts/Te);
          signal_filtre = filtre_reception_chaine2_bruit(signal_module, Ns, snr);
          suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 8, Ns);
        case 'chaine3'
          Ts = 2*Tb;
          Ns = floor(Ts/Te);
          signal_filtre = filtre_reception_chaine3_bruit(signal_module, Ns, snr);
          suite_binaire_reconstruite = decode_signal_filtre_4motifs(signal_filtre, Ns, Ns);
      end
      teb = autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite);
      y(i) = teb;
      i = i + 1;
    end

    TEB_exp = y;
  end

  function comparaison_chaines(suite_binaire, Fe, Rb)
    a = 0:8;
    x = 10.^(a / 10);

    figure();
    title("Comparaison TEB chaines 1 et 2");
    signal_module1 = modulateur('modulateur1', Fe, Rb, suite_binaire);
    TEB_exp1 = calcul_teb_exp('chaine1', suite_binaire, Fe, Rb, signal_module1, x);
    semilogy(a, TEB_exp1);
    hold on;
    signal_module2 = modulateur('modulateur1', Fe, Rb, suite_binaire);
    TEB_exp2 = calcul_teb_exp('chaine2', suite_binaire, Fe, Rb, signal_module2, x);
    semilogy(a, TEB_exp2);
    hold on;
    signal_module3 = modulateur('modulateur2', Fe, Rb, suite_binaire);
    TEB_exp3 = calcul_teb_exp('chaine3', suite_binaire, Fe, Rb, signal_module3, x);
    semilogy(a, TEB_exp3);
    legend('chaine 1', 'chaine2', 'chaine 3');
  end
