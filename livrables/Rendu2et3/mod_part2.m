function varargout = mod_part2(nom_fonction,varargin)
    switch nom_fonction
        case 'mod_QPSK'
            varargout = mod_QPSK(varargin{:});
        case 'etude_QPSK'
            etude_QPSK(varargin{:});
        case 'filtre_reception_QPSK'
            filtre_reception_QPSK(varargin{:});
        case 'comparaison_teb'
            comparaison_teb(varargin{:});
        case 'etude_part3_sortie_mod'
            etude_part3_sortie_mod(varargin{:});
        case 'reception_part3'
            reception_part3(varargin{:});
        case 'etude_part3'
            etude_part3(varargin{:});
        case 'etude_part4_sortie_mod'
            etude_part4_sortie_mod(varargin{:});
        case 'etude_part4'
            etude_part4(varargin{:});
        case 'comparaison_modulateurs'
            comparaison_modulateurs(varargin{:});
    end
end


function [signal_QPSK, I, Q] = mod_QPSK(Fe, Rb, fp, suite_binaire)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 2*Tb;
    Ns = floor(Ts/Te);

    a_k = suite_binaire(1:2:end)*2 - 1;
    b_k = suite_binaire(2:2:end)*2 - 1;
    motifs = a_k + 1j*b_k;
    h = rcosdesign(0.35,8,Ns);
    ordre = length(h);

    % on change la taille du signal pour pouvoir filtrer
    motifs = kron(motifs,[1 zeros(1,Ns-1)]);

    % On ajoute des zeros pour gerer le retard
    motifs = [motifs zeros(1,(ordre - 1)/2)];

    % on filtre
    signal_QPSK = filter(h,1,motifs);
    signal_QPSK = signal_QPSK((ordre - 1)/2 + 1:end);

    I = real(signal_QPSK);
    Q = imag(signal_QPSK);

    t = 0:Te:(length(signal_QPSK)-1)*Te;

    signal_QPSK = real(signal_QPSK .* exp(1j*2*pi*fp.*t));
end

function [enveloppe_complexe, I, Q, a_k, b_k] = mod_QPSK_part3(Fe, Rb, suite_binaire)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 2*Tb;
    Ns = floor(Ts/Te);

    a_k = suite_binaire(1:2:end)*2 - 1;
    b_k = suite_binaire(2:2:end)*2 - 1;
    motifs = a_k + 1j*b_k;
    h = rcosdesign(0.35,8,Ns);
    ordre = length(h);

    % on change la taille du signal pour pouvoir filtrer
    motifs = kron(motifs,[1 zeros(1,Ns-1)]);

    % On ajoute des zeros pour gerer le retard
    motifs = [motifs zeros(1,(ordre - 1)/2)];

    % on filtre
    enveloppe_complexe = filter(h,1,motifs);
    enveloppe_complexe = enveloppe_complexe((ordre - 1)/2 + 1:end);

    I = real(enveloppe_complexe);
    Q = imag(enveloppe_complexe);

end

function TEB = etude_QPSK(Fe, Rb, fp, suite_binaire, SNR, afficher)
    [signal_QPSK, I, Q] = mod_QPSK(Fe, Rb, fp, suite_binaire);

    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 2*Tb;
    Ns = floor(Ts/Te);


    t = 0:Te:(length(signal_QPSK)-1)*Te;
    
    if afficher
        figure();
        grid on;
        subplot(2,1,1);
        plot(t,I);
        legend('I(t)');
        subplot(2,1,2);
        plot(t,Q);
        legend('Q(t)');
        figure();
        plot(t,signal_QPSK);
    
        DSP_I = pwelch(I,[],[],[],Fe,'twosided');
        DSP_Q = pwelch(Q,[],[],[],Fe,'twosided');
        DSP_sig = pwelch(signal_QPSK,[],[],[],Fe,'twosided');
        grid on;
    
        F = linspace(-Fe/2,Fe/2,length(DSP_Q));
    
        figure();
        grid on;
        subplot(2,1,1);
        semilogy(F,fftshift(DSP_I));
        legend('DSP de I(t)');
        subplot(2,1,2);
        semilogy(F,fftshift(DSP_Q));
        legend('DSP de Q(t)');
    
        figure();
        grid on;
        semilogy(F,fftshift(DSP_sig));

    end

    signal_QPSK = signal_QPSK + bruit(signal_QPSK,Ns,4,SNR);
    I_reconstruit = signal_QPSK .* cos(2*pi*fp*t);
    Q_reconstruit = signal_QPSK .* sin(2*pi*fp*t);


    hr = rcosdesign(0.35,8,Ns);
    ordre = length(hr);

    motifs_reconstruits = I_reconstruit - 1j*Q_reconstruit;
    % On ajoute des zeros pour gerer le retard
    motifs_reconstruits = [motifs_reconstruits zeros(1,(ordre - 1)/2)];

    % on filtre
    motifs_reconstruits = filter(hr,1,motifs_reconstruits);
    motifs_reconstruits = motifs_reconstruits((ordre - 1)/2 + 1:end);

    % On échantillonne sur le premier bit
    motifs_reconstruits = reshape(motifs_reconstruits,Ns,[]);
    motifs_reconstruits = motifs_reconstruits(1,:); % instant echant = 1 car Nyquist respecté

    ak_reconstruits = (sign(real(motifs_reconstruits)) + 1) / 2;
    bk_reconstruits = (sign(imag(motifs_reconstruits)) + 1) / 2;

%     % On trace le diagramme de l'oeil avec la formule de l'énoncé
%     % On enlève les Ns premiers points pour eviter une ligne parasite
%     figure();
%     oeil = reshape(real(motifs_reconstruits), Ns, []);
%     plot(oeil(:,2:end));
%     title("Diagramme de l'oeil");
%     xlabel("indice du point");

    bits_retrouves = zeros(1,length(suite_binaire));
    bits_retrouves(1:2:end) = ak_reconstruits;
    bits_retrouves(2:2:end) = bk_reconstruits;

    TEB = autres('calculer_taux_erreur',suite_binaire,bits_retrouves);
    if afficher
        disp("TEB = " + TEB);
    end
end

function etude_part3_sortie_mod(Fe, Rb, suite_binaire)
    [signal_QPSK, I, Q, a_k, b_k] = mod_QPSK_part3(Fe, Rb, suite_binaire);
    Te = 1/Fe;

    t = 0:Te:(length(signal_QPSK)-1)*Te;

    figure();
    grid on;
    subplot(2,1,1);
    plot(t,I);
    legend('I(t)');
    subplot(2,1,2);
    plot(t,Q);
    legend('Q(t)');

    DSP_xe = pwelch(I + 1j*Q,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_xe));

    figure();
    grid on;
    semilogy(F,fftshift(DSP_xe));
    title("DSP de l'enveloppe complexe");

    figure();
    grid on;
    plot(a_k,b_k,"r+");
    xlim([-2 2]);
    ylim([-2 2]);
    xL = xlim;
    yL = ylim;
    line([0 0], yL,'Color','black');  %x-axis
    line(xL, [0 0],'Color','black');  %y-axis
    grid on;
    title("Constellation en sortie de mapping");

end

function signal_filtre = reception_part3(enveloppe_complexe, Fe, Rb, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 2*Tb;
    Ns = floor(Ts/Te);
    enveloppe_complexe = enveloppe_complexe + bruit(enveloppe_complexe,Ns,4,SNR) + 1j*bruit(enveloppe_complexe,Ns,4,SNR);

    hr = rcosdesign(0.35,8,Ns);
    ordre = length(hr);
    % On ajoute des zeros pour gerer le retard
    enveloppe_complexe = [enveloppe_complexe zeros(1,(ordre - 1)/2)];

    % on filtre
    signal_filtre = filter(hr,1,enveloppe_complexe);
    signal_filtre = signal_filtre((ordre - 1)/2 + 1:end);
end

function [bits_retrouves, motifs_reconstruits] = decode_part3(signal_filtre, Fe, Rb, suite_binaire)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 2*Tb;
    Ns = floor(Ts/Te);

    % On échantillonne sur le premier bit
    motifs_reconstruits = reshape(signal_filtre,Ns,[]);
    motifs_reconstruits = motifs_reconstruits(1,:); % instant echant = 1 car Nyquist respecté

    ak_reconstruits = (sign(real(motifs_reconstruits)) + 1) / 2;
    bk_reconstruits = (sign(imag(motifs_reconstruits)) + 1) / 2;


    bits_retrouves = zeros(1,length(suite_binaire));
    bits_retrouves(1:2:end) = ak_reconstruits;
    bits_retrouves(2:2:end) = bk_reconstruits;

end

function TEB = etude_part3(suite_binaire, Fe, Rb, SNR, afficher)
    if afficher
        etude_part3_sortie_mod(Fe, Rb, suite_binaire);
    end
    [enveloppe_complexe, ~, ~, ~, ~] = mod_QPSK_part3(Fe, Rb, suite_binaire);

    signal_filtre = reception_part3(enveloppe_complexe, Fe, Rb, SNR);
    [bits_retrouves, motifs_reconstruits] = decode_part3(signal_filtre, Fe, Rb, suite_binaire);


    if afficher
        figure();
        plot(real(motifs_reconstruits),imag(motifs_reconstruits),"r+");
        xlim([-2 2]);
        ylim([-2 2]);
        xL = xlim;
        yL = ylim;
        line([0 0], yL,'Color','black');  %x-axis
        line(xL, [0 0],'Color','black');  %y-axis
        grid on;
        title("Constellation en sortie de l'echantilloneur");
    
    %     % On trace le diagramme de l'oeil avec la formule de l'énoncé
    %     % On enlève les Ns premiers points pour eviter une ligne parasite
    %     figure();
    %     oeil = reshape(real(motifs_reconstruits), Ns, []);
    %     plot(oeil(:,2:end));
    %     title("Diagramme de l'oeil");
    %     xlabel("indice du point");
    end

    TEB = autres('calculer_taux_erreur',suite_binaire,bits_retrouves);
    if afficher
       disp("TEB = " + TEB);
    end

end

function [enveloppe_complexe, a_k, b_k] = mod_part4(Fe, Rb, suite_binaire)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = log2(8)*Tb;
    Ns = floor(Ts/Te);

    enveloppe_complexe = pskmod(suite_binaire', 8, pi/8, "gray", 'InputType', 'bit')';
    enveloppe_complexe = conj(enveloppe_complexe); % très important, on prend le conjugué

    a_k = real(enveloppe_complexe);
    b_k = imag(enveloppe_complexe);

    h = rcosdesign(0.2,8,Ns);
    ordre = length(h);

   % on change la taille du signal pour pouvoir filtrer
    enveloppe_complexe = kron(enveloppe_complexe,[1 zeros(1,Ns-1)]);

    % On ajoute des zeros pour gerer le retard
    enveloppe_complexe = [enveloppe_complexe zeros(1,(ordre - 1)/2)];

    % on filtre
    enveloppe_complexe = filter(h,1,enveloppe_complexe);
    enveloppe_complexe = enveloppe_complexe((ordre - 1)/2 + 1:end);
end

function etude_part4_sortie_mod(Fe, Rb, suite_binaire)
    [enveloppe_complexe, a_k, b_k] = mod_part4(Fe, Rb, suite_binaire);
   % Te = 1/Fe;
   % t = 0:Te:(length(signal_8PSK)-1)*Te;
    DSP_xe = pwelch(enveloppe_complexe,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_xe));

    figure();
    grid on;
    semilogy(F,fftshift(DSP_xe));
    title("DSP de l'enveloppe complexe");

    figure();
    grid on;
    plot(a_k,b_k,"r+");
    xlim([-2 2]);
    ylim([-2 2]);
    xL = xlim;
    yL = ylim;
    line([0 0], yL,'Color','black');  %x-axis
    line(xL, [0 0],'Color','black');  %y-axis
    grid on;
    title("Constellation en sortie de mapping");


end

function signal_filtre = reception_part4(enveloppe_complexe, Fe, Rb, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 3*Tb;
    Ns = floor(Ts/Te);
    enveloppe_complexe = enveloppe_complexe + ...
        bruit(enveloppe_complexe,Ns,8,SNR) + ...
        1j*bruit(enveloppe_complexe,Ns,8,SNR);

    hr = rcosdesign(0.2,8,Ns);
    ordre = length(hr);
    % On ajoute des zeros pour gerer le retard
    enveloppe_complexe = [enveloppe_complexe zeros(1,(ordre - 1)/2)];

    % on filtre
    signal_filtre = filter(hr,1,enveloppe_complexe);
    signal_filtre = signal_filtre((ordre - 1)/2 + 1:end);
end

function [bits_retrouves, motifs_reconstruits] = decode_part4(signal_filtre, Fe, Rb)
    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = 3*Tb;
    Ns = floor(Ts/Te);

    % On échantillonne sur le premier bit
    motifs_reconstruits = signal_filtre(1:Ns:end); % instant echant = 1 car Nyquist respecté

    bits_retrouves = pskdemod(motifs_reconstruits, 8, pi/8, "gray", "OutputType",'bit');
    bits_retrouves = reshape(bits_retrouves, 1, []);
end

function TEB = etude_part4(suite_binaire, Fe, Rb, SNR, afficher)
    if afficher
        etude_part4_sortie_mod(Fe, Rb, suite_binaire);
    end
    [enveloppe_complexe, ~, ~] = mod_part4(Fe, Rb, suite_binaire);

    signal_filtre = reception_part4(enveloppe_complexe, Fe, Rb, SNR);
    [bits_retrouves, motifs_reconstruits] = decode_part4(signal_filtre, Fe, Rb);


    if afficher
        figure();
        grid on;
        % plot(real(signal_filtre),imag(signal_filtre),"r+");
        plot(real(motifs_reconstruits),imag(motifs_reconstruits),"r+");
        xlim([-2 2]);
        ylim([-2 2]);
        xL = xlim;
        yL = ylim;
        line([0 0], yL,'Color','black');  %x-axis
        line(xL, [0 0],'Color','black');  %y-axis
        grid on;
        title("Constellation en sortie de l'echantilloneur");
    
        figure();
        a = 0:6;
        x = 10.^(a / 10);
    
        TEB = calcul_teb('chaine3', x);
        semilogy(a,TEB);
    
        hold on;
        TEB_exp = calcul_teb_exp('chaine3', suite_binaire, Fe, Rb, 2000, x);
        semilogy(a, TEB_exp);
        grid on;
        legend('theo 8psk', 'exp 8psk');
    
    end

    TEB = autres('calculer_taux_erreur',suite_binaire,bits_retrouves);
    if afficher
       disp("TEB = " + TEB);
    end

end

function comparaison_modulateurs(suite_binaire, Fe, Rb, fp)
    [enveloppe_complexe3, ~, ~, ~, ~] = mod_QPSK_part3(Fe, Rb, suite_binaire);
    [enveloppe_complexe4, ~, ~] = mod_part4(Fe, Rb, suite_binaire);
    DSP_xe3 = pwelch(enveloppe_complexe3,[],[],[],Fe,'twosided');
    DSP_xe4 = pwelch(enveloppe_complexe4,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_xe3));

    figure();
    grid on;
    
    semilogy(F,fftshift(DSP_xe3));
    hold on;
    semilogy(F,fftshift(DSP_xe4));
    grid on;
    title('Comparaison des efficacités spectrales des deux chaines');
    legend('Modulation QPSK', 'Modulation 8PSK');

    a = 0:6;
    x = 10.^(a / 10);

    TEB_exp3 = calcul_teb_exp('chaine2', suite_binaire, Fe, Rb, fp, x);
    TEB_exp4 = calcul_teb_exp('chaine3',suite_binaire, Fe, Rb, fp, x);
%     TEB_exp3 = calcul_teb('chaine1',x);
%     TEB_exp4 = calcul_teb('chaine3',x);


    figure();
    semilogy(a, TEB_exp3);
    hold on;
    semilogy(a,TEB_exp4);
    legend('Chaine QPSK', 'Chaine 8PSK');
    grid on;
    title('Comparaison des efficacités en puissance des deux chaines');
end

function comparaison_teb(suite_binaire, Fe, Rb, fp)
  figure();
  a = 0:6;
  x = 10.^(a / 10);

  TEB = calcul_teb('chaine1', x);
  semilogy(a,TEB);

  hold on;
  TEB_exp1 = calcul_teb_exp('chaine1', suite_binaire, Fe, Rb, fp, x);
  semilogy(a, TEB_exp1);
  legend('theo qpsk', 'exp qpsk');

  hold on;
  TEB_exp2 = calcul_teb_exp('chaine2', suite_binaire, Fe, Rb, fp, x);
  semilogy(a, TEB_exp2);
  legend('theo qpsk', 'exp qpsk', 'exp qpsk passe bas equivalent');
  grid on;
end

function TEB = calcul_teb(nom_chaine, SNR)
    switch nom_chaine
        case 'chaine1'
            TES = qfunc(sqrt(2*SNR));
            TEB = TES;
        case 'chaine3' % Chaine 8-PSK
            TES = 2*qfunc(sqrt(2*SNR*3)*sin(pi/8));
            TEB = TES/3;
    end
end

function TEB_exp = calcul_teb_exp(nom_chaine, suite_binaire, Fe, Rb, fp, SNR)
    y = zeros(1,length(SNR));
    i = 1;
    for snr = SNR
      switch nom_chaine
        case 'chaine1'
          teb = etude_QPSK(Fe, Rb, fp, suite_binaire, snr, 0);
        case 'chaine2'
          teb = etude_part3(suite_binaire, Fe, Rb, snr,0);
        case 'chaine3'
          teb = etude_part4(suite_binaire,Fe, Rb, snr, 0);
      end
      y(i) = teb;
      i = i + 1;
    end

    TEB_exp = y;
  end


function n = bruit(x,Ns,M,SNR)
    P_x = mean(abs(x).^2);
    sigma_n = sqrt(P_x*Ns/(2*log2(M)*SNR));
    n = sigma_n * randn(1,length(x));
end
