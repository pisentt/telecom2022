function varargout = mod_part3(nom_fonction,varargin)
    switch nom_fonction
        case 'modulateur1'
            varargout{1} = modulateur1(varargin{:});
        case 'modulateur_transition'
            varargout{1} = modulateur_transition(varargin{:});
        case 'decode_signal_filtre'
            varargout = decode_signal_filtre(varargin{:});
        case 'etude_demodulateur_1'
            etude_demodulateur_1(varargin{:});
        case 'etude_demodulateur_bruit'
            etude_demodulateur_bruit(varargin{:});
        case 'comparaison_teb'
            comparaison_teb(varargin{:});
        case 'etude_demodulateur_corrige'
            etude_demodulateur_corrige(varargin{:});
        case 'etude_demodulateur_corrige_transition'
            etude_demodulateur_corrige_transition(varargin{:});
        case 'comparaison_teb_correction'
            comparaison_teb_correction(varargin{:});
        case 'comparaison_teb_correction_transition'
            comparaison_teb_correction_transition(varargin{:});
    end
end

function signal_module = modulateur1(Fe, Rb, suite_binaire)

    Te = 1/Fe;
    Tb = 1/Rb;
    Ns = floor(Tb/Te);

    % on crée la suite de motifs (1 et -1)
    suite_motifs = suite_binaire*2 - 1;


    % on crée le filtre de mise en forme
    h_mise_en_forme = ones(1,Ns);

    % on change la taille du signal pour pouvoir filtrer
    suite_motifs = kron(suite_motifs,[1 zeros(1,Ns-1)]);

    % on filtre
    signal_module = filter(h_mise_en_forme,1,suite_motifs);
end

function signal_module = modulateur_transition(Fe, Rb, suite_binaire)
  suite_binaire_bip = suite_binaire*2 - 1;
  suite_transition = suite_binaire_bip;
  for i = 2:length(suite_transition)
    suite_transition(i) = suite_binaire_bip(i)*suite_transition(i-1);
  end
  % on retransforme les -1 en des 0 pour pouvoir utiliser l'ancien
  % modulateur tel quel. On fera l'opération inverse à la démodulation
  suite_transition = suite_transition > 0;
  signal_module = modulateur1(Fe, Rb, suite_transition);
end

function signal_filtre = filtre_reception_mod1(signal_module, Ns, phi)
  signal_module = signal_module * exp(1j*phi);
  hr = ones(1,Ns);

  % on filtre
  signal_filtre = filter(hr,1,signal_module);
end

function signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, SNR)
    signal_module = signal_module + bruit(signal_module,Ns,2,SNR) + 1j*bruit(signal_module,Ns,2,SNR);
    signal_filtre = filtre_reception_mod1(signal_module, Ns, phi);
end

function suite_binaire = decode_signal_filtre(signal_filtre, instant_echant, Ns, affiche)

  % On échantillonne sur le premier bit
  suite_binaire = reshape(signal_filtre,Ns,[]);

  if affiche
      figure();
      plot(real(suite_binaire(instant_echant,:)),imag(suite_binaire(instant_echant,:)),"r+");
      xlim([-5 5]);
      ylim([-5 5]);
      xL = xlim;
      yL = ylim;
      line([0 0], yL,'Color','black');  %x-axis
      line(xL, [0 0],'Color','black');  %y-axis
      grid on;
      title("Constellation en sortie d'echantilloneur");
  end

  suite_binaire = suite_binaire(instant_echant,:) > 0;
end

function suite_binaire = decode_signal_filtre_corrige(signal_filtre, instant_echant, Ns,affiche)

  % On échantillonne sur le premier bit
  suite_binaire = reshape(signal_filtre,Ns,[]);
    phi_estime = 0.5 * angle(sum(suite_binaire(instant_echant,:).^2));
    if affiche
        disp("Erreur de phase");
        disp(phi_estime);
    end
    suite_binaire = suite_binaire * exp(-1j*phi_estime);

  suite_binaire = suite_binaire(instant_echant,:) > 0;
end

function etude_demodulateur_corrige(suite_binaire, Fe, Rb, signal_module, phi, SNR)
  Te = 1/Fe;
  Tb = 1/Rb;

  Ts = Tb;
  Ns = floor(Ts/Te);
  signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, SNR);


  suite_binaire_reconstruite = decode_signal_filtre_corrige(signal_filtre, 4, Ns,1);


  disp("Taux erreur chaine avec bruit");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))
end

function suite_binaire_reconstruite = decodage_transition(suite_binaire_transition)
  % les -1 ont été transformés en 0 par l'ancien démodulateur. On les
  % retransforme donc en -1 pour appliquer la formule du décodage par
  % transition
  suite_transition_reconstruite = suite_binaire_transition*2 - 1;
  suite_binaire_reconstruite = suite_transition_reconstruite;
  for i = 2:length(suite_binaire_reconstruite)
    suite_binaire_reconstruite(i) = suite_transition_reconstruite(i)*suite_transition_reconstruite(i-1);
  end

  % convertion en suite binaire de 0 et de 1
  suite_binaire_reconstruite = suite_binaire_reconstruite > 0;
end

function etude_demodulateur_corrige_transition(suite_binaire, Fe, Rb, signal_module, phi, SNR)
  Te = 1/Fe;
  Tb = 1/Rb;

  Ts = Tb;
  Ns = floor(Ts/Te);
  signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, SNR);
  suite_binaire_transition_reconstruite = decode_signal_filtre_corrige(signal_filtre, 4, Ns,1);

  % decodage des transition
  suite_binaire_reconstruite = decodage_transition(suite_binaire_transition_reconstruite);

  disp("Taux erreur chaine avec bruit et codage transition");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))
end

function etude_demodulateur_bruit(suite_binaire, Fe, Rb, signal_module, phi, SNR)
  Te = 1/Fe;
  Tb = 1/Rb;

  Ts = Tb;
  Ns = floor(Ts/Te);
  signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, SNR);


  suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 4, Ns,1);


  disp("Taux erreur chaine avec bruit");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))
end

function etude_demodulateur_1(suite_binaire, Fe, Rb, signal_module,phi)
  Te = 1/Fe;
  Tb = 1/Rb;
  Ts = Tb;
  Ns = floor(Ts/Te);

  signal_filtre = filtre_reception_mod1(signal_module, Ns,phi);

  

  suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 4, Ns,1);


  disp("Taux erreur démodulateur 1");
  disp(autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite))

end

function comparaison_teb(suite_binaire, Fe, Rb,phi)
    figure();
    a = 0:6;
    x = 10.^(a / 10);
    
    signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
    TEB = calcul_teb(x,phi);
    semilogy(a, TEB);
    hold on;
    TEB_exp = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, phi, x);
    semilogy(a, TEB_exp);
    legend('theo chaine bruit phase', 'exp chaine bruit phase');

    figure();
    TEB_0 = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, 0, x);
    semilogy(a,TEB_0);
    hold on;
    TEB_40 = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, 0.7, x);
    semilogy(a,TEB_40);
    hold on;
    TEB_100 = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, 1.75, x);
    semilogy(a,TEB_100);
    legend('chaine sans phase', 'chaine phase 40','chaine phase 100');

end

function TEB = calcul_teb(SNR,phi)
    TES = qfunc(sqrt(2*SNR)*cos(phi));
    TEB = TES;
end

function comparaison_teb_correction(suite_binaire, Fe, Rb,phi)
    figure();
    a = 0:6;
    x = 10.^(a / 10);
    
    signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
    TEB = calcul_teb_exp_corrige(suite_binaire, Fe, Rb, signal_module, phi, x);
    semilogy(a, TEB);
    hold on;
    TEB_exp = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, phi, x);
    semilogy(a, TEB_exp);
    legend('teb corrige', 'teb non corrige');
end

function comparaison_teb_correction_transition(suite_binaire, Fe, Rb,phi)
    figure();
    a = 0:6;
    x = 10.^(a / 10);
    
    signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
    TEB = calcul_teb_exp_corrige(suite_binaire, Fe, Rb, signal_module, phi, x);
    semilogy(a, TEB);
    hold on;
    TEB_exp = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, phi, x);
    semilogy(a, TEB_exp);
    hold on;
    signal_module_transition = modulateur_transition(Fe, Rb, suite_binaire);
    TEB_exp_transition = calcul_teb_exp_transition(suite_binaire, Fe, Rb, signal_module_transition, phi, x);
    semilogy(a, TEB_exp_transition);
    legend('teb corrige', 'teb non corrige', 'teb corrige avec codage transition');
end

function TEB_exp = calcul_teb_exp(suite_binaire, Fe, Rb, signal_module, phi, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;
    
    y = zeros(1,length(SNR));
    i = 1;
    for snr = SNR
      Ts = Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, snr);
      suite_binaire_reconstruite = decode_signal_filtre(signal_filtre, 4, Ns,0);
    
      teb = autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite);
      y(i) = teb;
      i = i + 1;
    end
    
    TEB_exp = y;
end

function TEB_exp = calcul_teb_exp_corrige(suite_binaire, Fe, Rb, signal_module, phi, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;
    
    y = zeros(1,length(SNR));
    i = 1;
    for snr = SNR
      Ts = Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, snr);
      suite_binaire_reconstruite = decode_signal_filtre_corrige(signal_filtre, 4, Ns,0);
    
      teb = autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite);
      y(i) = teb;
      i = i + 1;
    end
    
    TEB_exp = y;
end

function TEB_exp = calcul_teb_exp_transition(suite_binaire, Fe, Rb, signal_module, phi, SNR)
    Te = 1/Fe;
    Tb = 1/Rb;
    
    y = zeros(1,length(SNR));
    i = 1;
    for snr = SNR
      Ts = Tb;
      Ns = floor(Ts/Te);
      signal_filtre = filtre_reception_bruit(signal_module, Ns, phi, snr);
      suite_binaire_transition_reconstruite = decode_signal_filtre_corrige(signal_filtre, 4, Ns,0);

      % decodage des transition
      suite_binaire_reconstruite = decodage_transition(suite_binaire_transition_reconstruite);
    
      teb = autres("calculer_taux_erreur", suite_binaire, suite_binaire_reconstruite);
      y(i) = teb;
      i = i + 1;
    end
    
    TEB_exp = y;
end

function n = bruit(x,Ns,M,SNR)
  P_x = mean(abs(x).^2);
  sigma_n = sqrt(P_x*Ns/(2*log2(M)*SNR));
  n = sigma_n * randn(1,length(x));
end
