% Ce fichier contient toutes les fonctions en lien avec la génération du
% signal modulé.

function varargout = modulateur(nom_fonction,varargin)

    switch nom_fonction
        case 'modulateur1'
            % Générer un signal avec le modulateur1 de la partie 1.
            [varargout{1}] = modulateur1(varargin{:});

        case 'etude_modulateur1'
            % Génerer les signaux théoriques et expérimentaux du
            % modulateur1 et les afficher.
            [varargout{1},varargout{2},varargout{3}] = etude_modulateur1(varargin{:});
           
        case 'modulateur2'
            % Générer un signal avec le modulateur2 de la partie 1.
            [varargout{1}] = modulateur2(varargin{:});

        case 'etude_modulateur2'
            % Génerer les signaux théoriques et expérimentaux du
            % modulateur2 et les afficher.
            [varargout{1},varargout{2},varargout{3}] = etude_modulateur2(varargin{:});

        case 'modulateur3'
            % Générer un signal avec le modulateur3 de la partie 1.
            [varargout{1}] = modulateur3(varargin{:});

        case 'etude_modulateur3'
            % Génerer les signaux théoriques et expérimentaux du
            % modulateur3 et les afficher.
            [varargout{1},varargout{2},varargout{3}] = etude_modulateur3(varargin{:});
    end

end



function signal_module = modulateur1(Fe, Rb, suite_binaire)

    Te = 1/Fe;
    Tb = 1/Rb;
    Ns = floor(Tb/Te);

    % on crée la suite de motifs (1 et -1)
    suite_motifs = suite_binaire*2 - 1;

    % on crée le filtre de mise en forme
    h_mise_en_forme = ones(1,Ns);

    % on change la taille du signal pour pouvoir filtrer
    suite_motifs = kron(suite_motifs,[1 zeros(1,Ns-1)]);

    % on filtre
    signal_module = filter(h_mise_en_forme,1,suite_motifs);
end


function [signal_module, DSP_exp, DSP_theo] = etude_modulateur1(Fe, Rb, suite_binaire)
    signal_module = modulateur1(Fe, Rb, suite_binaire);
    Te = 1/Fe;
    Ts = 1/Rb;
    t = 0:Te:(length(signal_module)-1)*Te;

    % On calcule les DSP.
    DSP_exp = pwelch(signal_module,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_exp));
    DSP_theo = 4*Ts*sinc(F*Ts).^2;

    % On affiche les signaux.
    figure();
    subplot(2,1,1);
    plot(t, signal_module);
    title("Signal du modulateur 1");
    xlabel("Temps (s)");
    ylabel("Puissance");

    subplot(2,1,2);
    semilogy(F,fftshift(DSP_exp));
    hold on;
    semilogy(F,DSP_theo);
    title("Comparaison des DSP du signal du modulateur 1");
    legend("DSP signal échantillonné", "DSP théorique");
    xlabel("Fréquence (Hz)");
    ylabel("Puissance");
end


function signal_module = modulateur2(Fe, Rb, suite_binaire)

    Te = 1/Fe;
    Tb = 1/Rb;
    Ts = log2(4)*Tb;
    Ns = floor(Ts/Te);

    % on crée la suite de motifs (-3, -1, 1, 3)
    % On fait un mapping de Grey
    suite_motifs1 = reshape(suite_binaire,2,[]) == [0;0];
    suite_motifs2 = reshape(suite_binaire,2,[]) == [0;1];
    suite_motifs3 = reshape(suite_binaire,2,[]) == [1;1];
    suite_motifs4 = reshape(suite_binaire,2,[]) == [1;0];

    suite_motifs1 = suite_motifs1(1,:) & suite_motifs1(2,:);
    suite_motifs2 = suite_motifs2(1,:) & suite_motifs2(2,:);
    suite_motifs3 = suite_motifs3(1,:) & suite_motifs3(2,:);
    suite_motifs4 = suite_motifs4(1,:) & suite_motifs4(2,:);

    suite_motifs = -3*suite_motifs1(1,:) - suite_motifs2(1,:) + ...
        suite_motifs3(1,:) + 3*suite_motifs4(1,:);


    % on crée le filtre de mise en forme
    h_mise_en_forme = ones(1,Ns);

    % on change la taille du signal pour pouvoir filtrer
    suite_motifs = kron(suite_motifs,[1 zeros(1,Ns-1)]);

    % on filtre
    signal_module = filter(h_mise_en_forme,1,suite_motifs);
end


function [signal_module, DSP_exp, DSP_theo] = etude_modulateur2(Fe, Rb, suite_binaire)
    signal_module = modulateur2(Fe, Rb, suite_binaire);
    Te = 1/Fe;
    t = 0:Te:(length(signal_module)-1)*Te;
    % On a 4 symboles donc Ts = 2 Tb.
    Tb = 1/Rb;
    Ts = 2*Tb;

    % On calcule les DSP.
    DSP_exp = pwelch(signal_module,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_exp));
    DSP_theo = 20*Ts*sinc(F*Ts).^2;

    % On affiche les signaux.
    figure();
    subplot(2,1,1);
    plot(t, signal_module);
    title("Signal du modulateur 2");
    xlabel("Temps (s)");
    ylabel("Puissance");

    subplot(2,1,2);
    semilogy(F,fftshift(DSP_exp));
    hold on;
    semilogy(F,DSP_theo);
    title("Comparaison des DSP du signal du modulateur 2");
    legend("DSP signal échantillonné", "DSP théorique");
    xlabel("Fréquence (Hz)");
    ylabel("Puissance");
end


function signal_module = modulateur3(Fe, Rb, alpha, suite_binaire)

    Te = 1/Fe;
    Tb = 1/Rb;
    Ns = floor(Tb/Te);

    % on crée la suite de motifs (1 et -1)
    suite_motifs = suite_binaire*2 - 1;

    % on crée le filtre de mise en forme
    h_mise_en_forme = rcosdesign(alpha,8,Ns);
    ordre = length(h_mise_en_forme);

    % on change la taille du signal pour pouvoir filtrer
    suite_motifs = kron(suite_motifs,[1 zeros(1,Ns-1)]);

    % On ajoute des zeros pour gerer le retard
    suite_motifs = [suite_motifs zeros(1,(ordre - 1)/2)];

    % on filtre
    signal_module = filter(h_mise_en_forme,1,suite_motifs);
    signal_module = signal_module((ordre - 1)/2 + 1:end);
end


function [signal_module, DSP_exp, DSP_theo] = etude_modulateur3(Fe, Rb, suite_binaire)
    Te = 1/Fe;
    Tb = 1/Rb;
    alpha = 0.25;

    signal_module = modulateur3(Fe, Rb, alpha, suite_binaire);
    t = 0:Te:(length(signal_module)-1)*Te;

    % On calcule les DSP.
    DSP_exp = pwelch(signal_module,[],[],[],Fe,'twosided');
    F = linspace(-Fe/2,Fe/2,length(DSP_exp));
    DSP_theo = DSP_theo_mod3(1, alpha, Tb, F);

    % On normalise la DSP_exp
    DSP_exp = DSP_exp / max(DSP_exp);

    % On affiche les signaux.
    figure();
    subplot(2,1,1);
    plot(t, signal_module);
    title("Signal du modulateur 3");
    xlabel("Temps (s)");
    ylabel("Puissance");

    subplot(2,1,2);
    semilogy(F,abs(fftshift(DSP_exp)));
    hold on;
    semilogy(F,abs(DSP_theo));
    title("Comparaison des DSP du signal du modulateur 3");
    legend("DSP signal échantillonné (normalisée)", "DSP théorique");
    xlabel("Fréquence (Hz)");
    ylabel("Puissance");
end


function DSP_theo = DSP_theo_mod3(sigma_a, alpha, Ts, F)
    Borne1 = (1 - alpha) / (2*Ts);
    Borne2 = (1 + alpha) / (2*Ts);
    K1 = sigma_a*sigma_a / Ts;
    K2 = pi*Ts/alpha;
    
    % On sépare le domaine de définition en 3 intervalles.
    % (on ne calcule que les deux intervalles où la DSP est non nulle)
    F1 = abs(F) <= Borne1;
    F2 = (Borne1 <= abs(F)) & (abs(F) <= Borne2);

    % On rassemble le tout.
    DSP_theo = K1*(Ts*F1 + Ts/2*(1+cos(K2*(abs(F) - Borne1))).*F2);
end
