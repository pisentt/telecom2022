clc;
clear;
close all;

%Variables définies
Fe = 24000;
Te = 1/Fe;
Rb = 3000;
Tb = 1/Rb;
BW = 8000;
Nbits = 64; % mettre au moins 10000 pour le calcul des TEB
            % attention, commenter les autres parties dans ce cas car ça
            % donne de très gros calculs.

%Modulateur 1
Ts = Te;

% % 0. Génération d'une suite binaire aléatoire
suite_binaire = autres('generer_suite_binaire',Nbits);


%% 1. Étude du signal NRZ(t) carré

[~,DSP_exp1,~] = modulateur('etude_modulateur1',Fe, Rb, suite_binaire);

[~,DSP_exp2,~] = modulateur('etude_modulateur2',Fe, Rb, suite_binaire);

[~,DSP_exp3,~] = modulateur('etude_modulateur3',Fe, Rb, suite_binaire);

 
% On affiche les signaux.
figure();
title("Comparaison des modulateurs");
xlabel("Temps (s)");
ylabel("Puissance");

semilogy(linspace(-Fe/2,Fe/2,length(DSP_exp1)),fftshift(DSP_exp1));
hold on;
semilogy(linspace(-Fe/2,Fe/2,length(DSP_exp2)),fftshift(DSP_exp2));
hold on;
semilogy(linspace(-Fe/2,Fe/2,length(DSP_exp3)),fftshift(DSP_exp3));
legend("Modulateur 1", "Modulateur 2", "Modulateur 3");


%% 3. Etude du démodulateur
% Sans canal
signal_module = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_demodulateur_sans_canal', suite_binaire, Fe, Rb, signal_module);

% Avec canal
signal_module_canal = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_demodulateur_avec_canal', suite_binaire, Fe, Rb, signal_module_canal, BW);

%% Dans le cas BW = 8000Hz, le canal n'a aucun effet sur le TEB. On a bien un TEB nul

%% On constate que si BW = 1000Hz alors le canal coupe le "lobe principal" de la réponse fréquentielle H(f)Hr(f) donc on perd de l'info.
% Dans ce cas on a un TEB != 0


%% 4. Etude de chaque chaine de transmission

% chaine 1

signal1 = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_transmission', suite_binaire, 'chaine1', Fe, Rb, signal1);

% chaine 2

signal2 = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_transmission', suite_binaire, 'chaine2', Fe, Rb, signal2);

% chaine 3

signal3 = modulateur('modulateur2', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_transmission', suite_binaire, 'chaine3', Fe, Rb, signal3);

%% 4. Etude avec le bruit
SNR = 20; %Eb/N0, faire varier le SNR pour obtenir les figures du rapport
%% chaine 1
signal1_bruit = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_bruit', suite_binaire, 'chaine1',Fe, Rb, signal1_bruit, SNR);

%% chaine 2
signal2_bruit = modulateur('modulateur1', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_bruit', suite_binaire, 'chaine2', Fe, Rb, signal2_bruit, SNR);

%% chaine 3
signal3_bruit = modulateur('modulateur2', Fe, Rb, suite_binaire);
demodulateur('etude_chaine_bruit', suite_binaire, 'chaine3', Fe, Rb, signal3_bruit, SNR);

TEB = demodulateur('calcul_teb','chaine1',0 : 0.1 : 8);
semilogy(TEB);

demodulateur('comparaison_teb', suite_binaire, Fe, Rb, "chaine1");
demodulateur('comparaison_teb', suite_binaire, Fe, Rb, "chaine2");
demodulateur('comparaison_teb', suite_binaire, Fe, Rb, "chaine3");

demodulateur('comparaison_chaines', suite_binaire, Fe, Rb);
