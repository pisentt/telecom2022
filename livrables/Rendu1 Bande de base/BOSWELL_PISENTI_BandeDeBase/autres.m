% Ce fichier contient des fonctions utiles qui n'avaient pas leur place
% dans les autres fichiers.

function varargout = autres(nom_fonction,varargin)

    switch nom_fonction
        case 'calculer_taux_erreur'
            [varargout{1}] = calculer_taux_erreur(varargin{:});
        case 'generer_suite_binaire'
            [varargout{1}] = generer_suite_binaire(varargin{:});
    end

end


function suite_binaire = generer_suite_binaire(Nbits)

    suite_binaire = randi([0 1],1,Nbits);
end

function taux_erreur = calculer_taux_erreur (suite_binaire, suite_binaire_reconstruite)

    nb_bits_erreur = sum(abs(suite_binaire - suite_binaire_reconstruite));
    taux_erreur = nb_bits_erreur/length(suite_binaire);
end