# formules mathématiques pour le rapport de télécom Partie 1

## densité spectrale de puissance du modulateur 1

$$\begin{aligned}
S_x(f) &= \dfrac {\sigma_a^2} {T_s} | H(f)| ^2 + 
\underbrace{A \sum \text{Re}(R_a(k)e^-m) + |m_a|^2 B}
    _ {= 0 \text{ car signal centré et uniformément distribué}} \\
&= \dfrac 1 {T_s} | \text{TF}(h(t))|^2 \text{ car } \sigma_a^2 = E(X^2) - 
    \underbrace{E(X)^2}_ {=0 \text{car signal centré}} = 1 \\
&= \dfrac 1 {T_s} |2T_s \text{sinc}(2\pi f T_s)|^2 \text{ car } h(t) = \Pi_{T_s}(t) \\
&= 4T_s \text{sinc}^2(2\pi f T_s)
\end{aligned}$$

## densité spectrale de puissance du modulateur 2

Le signal étant centré et uniformément distribué on a :
$$\begin{aligned}
S_x(f) &= \dfrac {\sigma_a^2} {T_s} | H(f) | ^2 \\
&= \dfrac 5 {T_s} | \text{TF}(h(t))|^2 \text{ car } \sigma_a^2 = E(X^2) -
    \underbrace{E(X)^2}_ {=0 \text{car signal centré}} = \dfrac{9 + 1 + 1 + 9} 4 = 5\\
&= \dfrac 5 {T_s} |2T_s \text{sinc}(2\pi f T_s)|^2 \text{ car } h(t) = \Pi_{T_s}(t) \\
&= 20 T_s \text{sinc}^2 (2\pi f T_s)
\end{aligned}$$
