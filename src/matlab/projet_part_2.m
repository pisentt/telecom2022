clc;
clear;
close all;

%Variables définies
Fe = 24000;
fp = 2000;
% Te = 1/Fe;
Rb = 3000;
% Tb = 1/Rb;
% BW = 1000;
Nbits = 60000;

suite_binaire = autres('generer_suite_binaire',Nbits);


%% 2. Implantation de la transmission

% SNR = 100;
% mod_part2('etude_QPSK',Fe, Rb, fp, suite_binaire, SNR, 1);
% mod_part2('comparaison_teb',suite_binaire, Fe, Rb, fp);

%% 3. Chaine passe bas equivalente
 
% SNR = 100;
% mod_part2('etude_part3',suite_binaire, Fe, Rb, SNR, 1);
% mod_part2('comparaison_teb',suite_binaire, Fe, Rb, fp);

%% 4. Comparaison modulateurs

Fe = 6000;
SNR = 10;
mod_part2('etude_part4',suite_binaire, Fe, Rb, SNR, 1);

mod_part2('comparaison_modulateurs', suite_binaire, Fe, Rb, fp);
