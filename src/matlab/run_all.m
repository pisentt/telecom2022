% Ceci est un fichier d'exemple qui va générer toutes les figures utilisées
% dans le rapport.

clc;
close all;
clear;

% Tous les paramètres du fichier sont déclarés ici.
Nbits = 64;
Rb = 300;
Fe = 48000;
Fc = 1080;
N_FFT = 2048;
delta_f = 100;
F0 = Fc + delta_f;
F1 = Fc - delta_f;
phi0 = rand*2*pi;
phi1 = rand*2*pi;
SNR = 50;
ordre = 201;
Te = 1 / Fe;

% Pour la partie 5 du sujet, décommenter ces lignes.
% F0 = 6000;
% F1 = 2000;
% Fc = (F0 + F1)/2;


% 0. Génération d'une suite binaire aléatoire
suite_binaire = autres('generer_suite_binaire',Nbits);



% 1. Étude du signal NRZ(t) carré

[NRZ, DSP_NRZ, SNRZ] = modulateur('etude_signal_NRZ',Fe, Rb, suite_binaire);
t_NRZ = 0:Te:(length(NRZ)-1)*Te;
F_NRZ = linspace(-Fe/2,Fe/2,length(DSP_NRZ));

figure(1);
plot(t_NRZ, NRZ);
xlabel("temps (s)");
ylabel("valeur du bit");
title("signal NRZ");
grid on;

figure(2);
semilogy(F_NRZ,abs(fftshift(DSP_NRZ)));
hold on;
semilogy(F_NRZ,SNRZ);
title("Comparaison des DSP du signal NRZ");
legend("DSP signal échantillonné", "DSP théorique");
xlabel("Fréquence (Hz)");
grid on;
